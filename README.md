# dog classifier--new

Dog classifier trained on images of 133 dog breeds. Built using pretrained VGGnet Feature extraction and FF Neural Network classifier.  python, pytorch, and other anaconda libraries

Inlcudes a CNN Training model to train on custom dog image dataset or improve on the current model accuracy of 90%


This project can be run on the command line, or used a python package.

## Commandline

run ***python dog_classifier.py --h*** 
for list of commands


## Web Interface

*Coming soon*