###############################
# get commandline arguements  #
###############################

import argparse

def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('input_image_path', type = str, help = 'path to the dog image')

    return parser.parse_args()
