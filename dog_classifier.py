import json
import numpy as np
from PIL import Image
from model_training.model_architecture import model_arch
import torch
import torchvision.transforms as transforms
import commandline_args as cl




def predict_dog_breed(img_path, device):
    with open('./model_training/data_classes.json') as json_file:
        class_names = json.load(json_file)

        image = Image.open(img_path)

        in_transform = transforms.Compose([
                            transforms.Resize(256),
                            transforms.CenterCrop(224),
                            transforms.ToTensor(),
                            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                            ])
        image = in_transform(image).unsqueeze(0).to(device)

        model = model_arch().to(device)
        model.load_state_dict(torch.load('./model_training/model.pt', map_location=device))
        out = model(image)

        value, indice = torch.nn.functional.softmax(out[0], dim=0).max(0)

        return class_names[indice.cpu().numpy()] # predicted class index


if __name__ == '__main__':

    cl_input = cl.get_args()

    path_to_image = cl_input.input_image_path
    device = torch.device('cpu')

    output = predict_dog_breed(path_to_image, device)

    print(f'The predicted breed of this image: {output}')
