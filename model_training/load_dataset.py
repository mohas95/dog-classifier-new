import os
import torch
from torchvision import datasets
from PIL import ImageFile
import torchvision.transforms as transforms
import json
ImageFile.LOAD_TRUNCATED_IMAGES = True # fixes truncated image problem


def load_data(img_data_root):
    process_transform = transforms.Compose([
                        transforms.Resize(256),
                        transforms.CenterCrop(224),
                        transforms.ToTensor(),
                        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
                        ])
    augment_transform = transforms.Compose([
                        transforms.RandomResizedCrop(224),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor(),
                        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                        ])

    train_data = datasets.ImageFolder(img_data_root + 'train/', transform=augment_transform)
    train_loader = torch.utils.data.DataLoader(train_data, batch_size = 64, shuffle = True)

    valid_data = datasets.ImageFolder(img_data_root + 'valid/', transform=process_transform)
    valid_loader = torch.utils.data.DataLoader(valid_data, batch_size = 64, shuffle = True)

    test_data = datasets.ImageFolder(img_data_root + 'test/', transform=process_transform)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size = 64, shuffle = True)

    data = {'train': train_data, 'valid': valid_data, 'test': test_data}
    loader = {'train': train_loader, 'valid': valid_loader, 'test': test_loader}

    return data, loader

def export_classes(data_root):

    data, loader = load_data(data_root)
    print(data['train'])
    class_names = [item[4:].replace("_", " ") for item in data['train'].classes]

    with open('data_classes.json', 'w') as write_file:
        json.dump(class_names, write_file)

    return class_names
